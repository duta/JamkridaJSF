package id.co.ptdmc.jamkrida.models;

public class Role extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7843026812555045280L;

	private String id;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
