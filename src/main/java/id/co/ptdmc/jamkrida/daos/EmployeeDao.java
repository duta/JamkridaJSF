package id.co.ptdmc.jamkrida.daos;

import id.co.ptdmc.jamkrida.models.Employee;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class EmployeeDao extends BaseDao<Employee> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5584888614298127976L;

    @Transactional
	public List<Employee> getAll() {
		List<Employee> employees = getEntityManager().createQuery("select e from Employee e").getResultList();
		return employees;
	}

}
