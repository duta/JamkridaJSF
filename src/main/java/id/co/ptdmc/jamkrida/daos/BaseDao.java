package id.co.ptdmc.jamkrida.daos;

import id.co.ptdmc.jamkrida.models.BaseModel;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class BaseDao<X extends BaseModel> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8022195967892203942L;

	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
