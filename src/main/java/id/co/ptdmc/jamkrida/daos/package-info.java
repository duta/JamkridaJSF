/**
 * A package that contains all Data Access Object classes.
 *
 * @author Rochmat Santoso
 *
 */
package id.co.ptdmc.jamkrida.daos;
