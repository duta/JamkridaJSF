/**
 * A package that contains all utility classes.
 *
 * @author Rochmat Santoso
 *
 */
package id.co.ptdmc.jamkrida.utils;
