package id.co.ptdmc.jamkrida.utils;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import id.co.ptdmc.jamkrida.services.CommonService;

public class ServiceUtil {

	private ServiceUtil() {
	}

	private static WebApplicationContext getContext() {
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        return ctx;
	}

	public static CommonService getCommonService() {
        return (CommonService) getContext().getBean("commonService");
	}

}
