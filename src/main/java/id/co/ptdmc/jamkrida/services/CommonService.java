package id.co.ptdmc.jamkrida.services;

import id.co.ptdmc.jamkrida.daos.EmployeeDao;
import id.co.ptdmc.jamkrida.daos.RoleDao;
import id.co.ptdmc.jamkrida.daos.UserInfoDao;
import id.co.ptdmc.jamkrida.models.Employee;
import id.co.ptdmc.jamkrida.models.Role;
import id.co.ptdmc.jamkrida.models.UserInfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommonService extends BaseService<Employee> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8303831682759538976L;

	private EmployeeDao employeeDao;
	private RoleDao roleDao;
	private UserInfoDao userInfoDao;

	/**
	 * @return the employeeDao
	 */
	public EmployeeDao getEmployeeDao() {
		return employeeDao;
	}

	/**
	 * @param employeeDao the employeeDao to set
	 */
	@Autowired
	public void setEmployeeDao(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}

	/**
	 * @return the roleDao
	 */
	public RoleDao getRoleDao() {
		return roleDao;
	}

	/**
	 * @param roleDao the roleDao to set
	 */
	@Autowired
	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	/**
	 * @return the userInfoDao
	 */
	public UserInfoDao getUserInfoDao() {
		return userInfoDao;
	}

	/**
	 * @param userInfoDao the userInfoDao to set
	 */
	@Autowired
	public void setUserInfoDao(UserInfoDao userInfoDao) {
		this.userInfoDao = userInfoDao;
	}

	public List<Employee> getAllEmployee() {
		List<Employee> employees = employeeDao.getAll();
		return employees;
	}

	public List<Role> getRoleByUsername(String username) {
		List<Role> roles = roleDao.getByUsername(username);
		return roles;
	}

	public List<Role> getRoleByViewId(String viewId) {
		List<Role> roles = roleDao.getByView(viewId);
		return roles;
	}

	public UserInfo getUserInfoAfterLogin(String username) {
		List<Role> userRoles = getRoleByUsername(username);
		UserInfo userInfo = userInfoDao.getByUsername(username);
		userInfo.setRoles(userRoles);

		return userInfo;
	}

}
