/**
 * A package that contains all service classes.
 *
 * @author Rochmat Santoso
 *
 */
package id.co.ptdmc.jamkrida.services;
