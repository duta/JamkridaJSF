package id.co.ptdmc.jamkrida.controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class ViewEmployeesManagedBean1 extends BaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9143815298675282343L;

	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	protected String getViewId() {
		return "EmployeeResult";
	}

}
