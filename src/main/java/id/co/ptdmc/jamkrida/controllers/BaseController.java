package id.co.ptdmc.jamkrida.controllers;

import id.co.ptdmc.jamkrida.models.Role;
import id.co.ptdmc.jamkrida.utils.ServiceUtil;

import java.io.Serializable;
import java.util.List;

public abstract class BaseController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9064753468673302806L;

	protected abstract String getViewId();

	protected boolean isAuthorized(List<Role> userRoles) {
		boolean result = false;

		List<Role> allowedRoles = ServiceUtil.getCommonService().getRoleByViewId(getViewId());
		for (Role obj : userRoles) {
			String roleId = obj.getId();
			if (allowedRoles.contains(roleId)) {
				result = true;
				break;
			}
		}

		return result;
	}

}
