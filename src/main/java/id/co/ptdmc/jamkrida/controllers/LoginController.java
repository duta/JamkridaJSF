package id.co.ptdmc.jamkrida.controllers;

import id.co.ptdmc.jamkrida.models.UserInfo;
import id.co.ptdmc.jamkrida.utils.CommonUtil;
import id.co.ptdmc.jamkrida.utils.ServiceUtil;

import java.io.IOException;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;

@ManagedBean
@RequestScoped
public class LoginController extends BaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6765773251761830811L;
	private static final Logger logger = Logger.getLogger(LoginController.class);

	private String username;
	private String password;

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	public void doLogin() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        RequestDispatcher dispatcher = ((ServletRequest) context.getRequest()).getRequestDispatcher("/login");
        try {
			dispatcher.forward((ServletRequest) context.getRequest(), (ServletResponse) context.getResponse());

			UserInfo userInfo = ServiceUtil.getCommonService().getUserInfoAfterLogin(username);
			Map<String, Object> sessionMap = context.getSessionMap();
			sessionMap.put(CommonUtil.SESSION_USERNAME, userInfo.getUsername());
			sessionMap.put(CommonUtil.SESSION_NAME, userInfo.getName());
			
			logger.info(userInfo.getName() + " logged in");
		} catch (ServletException | IOException e) {
			logger.error("Error", e);
		}
        FacesContext.getCurrentInstance().responseComplete();
	}

	public void doLogout() {
		try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/jamkrida-jsf");
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

	@Override
	protected String getViewId() {
		return "Login";
	}

}
