package id.co.ptdmc.jamkrida.controllers;

import id.co.ptdmc.jamkrida.models.Employee;
import id.co.ptdmc.jamkrida.utils.ServiceUtil;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

@ManagedBean
@ViewScoped
public class ViewEmployeesManagedBean extends BaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6154320068193802270L;

	private List<Employee> employees;

	public ViewEmployeesManagedBean() {
		this.employees = new ArrayList<Employee>();
	}

	@PostConstruct
	public void populateEmployeeList() {
		System.out.println("Buat bean...");
		this.employees = ServiceUtil.getCommonService().getAllEmployee();
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public void delete(ActionEvent event) {
		Employee employee = (Employee) event.getComponent().getAttributes().get("selected");
		System.out.println(employee.getName());
	}

	@Override
	protected String getViewId() {
		return "EmployeeGrid";
	}

}
